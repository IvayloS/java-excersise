package com.citi.training.stocks.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stocks.model.Stock;
import com.citi.training.stocks.model.StockDao;

@RestController
@RequestMapping("/stock")
public class StockController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

    @Autowired
    StockDao stockDao;

    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Stock> findAll(){
    	LOG.warn("Some message");;
    	LOG.debug("Message for devs");
        return stockDao.findAll();
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public Stock create(@RequestBody Stock stock) {
    	stockDao.save(stock);
    	return stock;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Stock findById(@PathVariable long id) {
    	return stockDao.findById(id).get();
    }
    
    @RequestMapping(value="/remove/{id}", method=RequestMethod.DELETE)
    public @ResponseBody void deleteById(@PathVariable long id) {
    	stockDao.deleteById(id);
    }
}
