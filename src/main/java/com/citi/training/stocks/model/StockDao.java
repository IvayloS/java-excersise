package com.citi.training.stocks.model;

import org.springframework.data.repository.CrudRepository;

public interface StockDao extends CrudRepository<Stock, Long> {

}
